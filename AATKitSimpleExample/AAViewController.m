#import "AAViewController.h"

static NSString* const kPlacementNameBanner = @"BannerPlacement";
static NSString* const kPlacementNameFullscreen = @"FullscreenPlacement";
static NSString* const kPlacementNameRewardedVideo = @"RewardedVideoPlacement";

@interface AAViewController ()

@property (strong, nonatomic) id bannerPlacement;
@property (strong, nonatomic) id fullscreenPlacement;
@property (strong, nonatomic) id rewardedVideoPlacement;

@end


@implementation AAViewController

- (void)viewDidLoad
{
#ifdef DEBUG
    // Optionally enable AATKit debug logs.
    [AATKit debug:YES];
    
    // Use AATKit test ads during development.
    // Register your app bundle id on AddApptr server to get real ads
    // in release version of your app.
    [AATKit initWithViewController:self delegate:self andEnableTestModeWithID:154];
#else
    // Initialize AATKit before creating any placements. Initialize it only once.
    // Pass a UIViewController that presents / dismisses other ViewControllers
    // and a delegate to listen for AATKit events if needed.
    [AATKit initWithViewController:self andDelegate:self];
#endif
    

    
    // Create banner placement
    {
        // Choose appropriate placement type. Various sizes exist.
        AATKitAdType placementType;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            placementType = AATKitBanner768x90;
        } else {
            CGRect screenRect   = [[UIScreen mainScreen] bounds];
            CGFloat screenWidth = screenRect.size.width;
            if (screenWidth == 414.0) {
                placementType = AATKitBanner414x53; // wrapper for 320x53
            }
            else
            if (screenWidth == 375.0) {
                placementType = AATKitBanner375x53; // wrapper for 320x53
            }
            else {
                placementType = AATKitBanner320x53;
             }
        }
        
        // Create placement.
        self.bannerPlacement = [AATKit createPlacementWithName:kPlacementNameBanner andType:placementType];
        
        // Add banner placement view to self.view
        // Get placement view, ...
        UIView *bannerPlacementView = [AATKit getPlacementView:self.bannerPlacement];
        
        // ... place it at the bottom, ...
        CGPoint pos = CGPointMake(0,
                                  self.view.frame.size.height - bannerPlacementView.frame.size.height);
        [AATKit setPlacementPos:pos forPlacement:self.bannerPlacement];
        
        // ... make it stay there even when superview size changes ...
        bannerPlacementView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
        
        // ... and add it on top of view hierarchy.
        [self.view addSubview:bannerPlacementView];
        [self.view bringSubviewToFront:bannerPlacementView];
    }
    
    // Create fullscreen placement
    {
        // Use AATKitFullscreen type for fullscreen placements.
        self.fullscreenPlacement = [AATKit createPlacementWithName:kPlacementNameFullscreen
                                                           andType:AATKitFullscreen];
        
        // Start fullscreen placement auto-reloading to automatically
        // download new fullscreen ad after presenting the current one.
        [AATKit startPlacementAutoReload:self.fullscreenPlacement];
    }
    
    // Create rewarded video placement
    {
        // Use AATKitFullscreen type for fullscreen placements.
        self.rewardedVideoPlacement = [AATKit createPlacementWithName:kPlacementNameRewardedVideo
                                                           andType:AATKitFullscreen];
        
        // Please note: rewarded videos need a special setup
        // on our dashboard at dashboard.addapptr.com
        // Please send an email containing the name of your
        // placement (e.g. "RewardedVideoPlacement") to suppor@addapptr.com
        // and tell us, that you want to use it for rewareded videos.
        
        // Start fullscreen placement auto-reloading to automatically
        // download new fullscreen ad after presenting the current one.
        [AATKit startPlacementAutoReload:self.rewardedVideoPlacement];
        
        [self.watchRewardedVideoPlacementButton setEnabled:NO];
        
    }
    
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Start banner placement auto-reloading to download new ads automatically.
    // Download new ads only when placement is visible to reduce network traffic.
    [AATKit startPlacementAutoReload:self.bannerPlacement];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [AATKit stopPlacementAutoReload:self.bannerPlacement];
    [super viewDidDisappear:animated];
}

- (IBAction)onShowFullscreenPlacementButtonClicked:(UIButton *)sender
{
    //show the fullscreen placement when button is clicked
    bool adCouldBePresented = [AATKit showPlacement:self.fullscreenPlacement];
	
    if (!adCouldBePresented) {
        [self showAlertViewWithTitle:@"Ad has not been loaded yet!"
                          andMessage:@"Please wait for ad to be loaded and press again."];
    }
    self.showFullscreenPlacementButton.enabled = [AATKit haveAdForPlacement:self.fullscreenPlacement];
}

- (void) showAlertViewWithTitle:(NSString*)title andMessage:(NSString*)message
{
	UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title
														message:message
													   delegate:self
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
	[alertView show];
}

- (IBAction)onWatchRewardedVideoPlacementButtonClicked:(UIButton *)sender
{
    NSLog(@"Watch rewarded video button clicked.");
    
    // deactivate watch rewarded video button until next ad has been loaded
    [self.watchRewardedVideoPlacementButton setEnabled:NO];
    
    //show the fullscreen placement when button is clicked
    [AATKit showPlacement:self.rewardedVideoPlacement];
}

#pragma mark AATKitDelegate Implementation

- (void)AATKitHaveAd:(id)placement
{
    NSLog(@"Placement %@ is populated with Ads.", placement);
    self.showFullscreenPlacementButton.enabled = [AATKit haveAdForPlacement:self.fullscreenPlacement];

    // activate watch rewarded video button if rewarded video is loaded
    self.watchRewardedVideoPlacementButton.enabled = [AATKit haveAdForPlacement:self.rewardedVideoPlacement];
}

- (void) AATKitNoAds:(id) placement
{
    NSLog(@"Placement %@ is not populated with Ads.", placement);
}

- (void) AATKitShowingEmpty:(id) placement
{
    NSLog(@"AATKitShowingEmpty called");
}

- (void) AATKitPauseForAd
{
    NSLog(@"The application using AATKit has paused in order to display Ads.");
}

- (void) AATKitResumeAfterAd
{
    NSLog(@"The application wants to resume after displaying an Ad");
}

- (void) AATKitUserEarnedIncentiveOnPlacement:(id) placement
{
    NSLog(@"User earned incentive on placement %@.", placement);
    
}
@end
