
#import <UIKit/UIKit.h>

#import <AATKit/AATKit.h>


@interface AAViewController : UIViewController<AATKitDelegate>

@property (weak, nonatomic) IBOutlet UIButton *showFullscreenPlacementButton;
@property (weak, nonatomic) IBOutlet UIButton *watchRewardedVideoPlacementButton;

@end
